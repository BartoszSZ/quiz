var $collectionHolder;

jQuery(document).ready(function() {
    $collectionHolder = $('ul.tags');

    $collectionHolder.find('li').each(function() {
        addAnswerFormDeleteLink($(this));
    });

    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $('#a_add_answer').on('click', function(e) {
        e.preventDefault();
        addAnswerForm($collectionHolder);
    });
});

function addAnswerForm($collectionHolder) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li></li>').append(newForm);
    $collectionHolder.append($newFormLi);

    addAnswerFormDeleteLink($newFormLi);
}

function addAnswerFormDeleteLink($tagFormLi) {
    var $removeFormA = $('<a href="#">Usuń odpowiedź</a>');
    $tagFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        e.preventDefault();

        $tagFormLi.remove();
    });
}
