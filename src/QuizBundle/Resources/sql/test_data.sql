--
-- Zrzut danych tabeli `questions`
--

INSERT INTO `questions` (`id`, `identifier`, `content`, `type`, `level`, `status`) VALUES
(1, 'Pytanie 1', 'Treść pytania numer 1?', 2, 2, 1),
(2, 'Pytanie 2', 'Treść pytania numer 2?', 1, 3, 1),
(3, 'Pytanie 3', 'Treść pytania numer 3?', 2, 1, 1),
(4, 'Pytanie 4', 'Treść pytania numer 4?', 1, 2, 1),
(5, 'Pytanie 5', 'Treść pytania numer 5?', 2, 3, 1),
(6, 'Pytanie 6', 'Treść pytania numer 6?', 1, 1, 1),
(7, 'Pytanie 7', 'Treść pytania numer 7?', 2, 2, 1),
(8, 'Pytanie 8', 'Treść pytania numer 8?', 1, 3, 1),
(9, 'Pytanie 9', 'Treść pytania numer 9?', 2, 1, 1),
(10, 'Pytanie 10', 'Treść pytania numer 10?', 1, 2, 1),
(11, 'Pytanie 11', 'Treść pytania numer 11?', 2, 3, 1),
(12, 'Pytanie 12', 'Treść pytania numer 12?', 1, 1, 1),
(13, 'Pytanie 13', 'Treść pytania numer 13?', 2, 2, 1),
(14, 'Pytanie 14', 'Treść pytania numer 14?', 1, 3, 1),
(15, 'Pytanie 15', 'Treść pytania numer 15?', 2, 1, 1),
(16, 'Pytanie 16', 'Treść pytania numer 16?', 1, 2, 1),
(17, 'Pytanie 17', 'Treść pytania numer 17?', 2, 3, 1),
(18, 'Pytanie 18', 'Treść pytania numer 18?', 1, 1, 1),
(19, 'Pytanie 19', 'Treść pytania numer 19?', 2, 2, 1),
(20, 'Pytanie 20', 'Treść pytania numer 20?', 1, 3, 1),
(21, 'Pytanie 21', 'Treść pytania numer 21?', 2, 1, 1),
(22, 'Pytanie 22', 'Treść pytania numer 22?', 1, 2, 1),
(23, 'Pytanie 23', 'Treść pytania numer 23?', 2, 3, 1),
(24, 'Pytanie 24', 'Treść pytania numer 24?', 1, 1, 1),
(25, 'Pytanie 25', 'Treść pytania numer 25?', 2, 2, 1),
(26, 'Pytanie 26', 'Treść pytania numer 26?', 1, 3, 1),
(27, 'Pytanie 27', 'Treść pytania numer 27?', 2, 1, 1),
(28, 'Pytanie 28', 'Treść pytania numer 28?', 1, 2, 1),
(29, 'Pytanie 29', 'Treść pytania numer 29?', 2, 3, 1),
(30, 'Pytanie 30', 'Treść pytania numer 30?', 1, 1, 1),
(31, 'Pytanie 31', 'Treść pytania numer 31?', 2, 2, 1),
(32, 'Pytanie 32', 'Treść pytania numer 32?', 1, 3, 1),
(33, 'Pytanie 33', 'Treść pytania numer 33?', 2, 1, 1),
(34, 'Pytanie 34', 'Treść pytania numer 34?', 1, 2, 1),
(35, 'Pytanie 35', 'Treść pytania numer 35?', 2, 3, 1),
(36, 'Pytanie 36', 'Treść pytania numer 36?', 1, 1, 1),
(37, 'Pytanie 37', 'Treść pytania numer 37?', 2, 2, 1),
(38, 'Pytanie 38', 'Treść pytania numer 38?', 1, 3, 1),
(39, 'Pytanie 39', 'Treść pytania numer 39?', 2, 1, 1),
(40, 'Pytanie 40', 'Treść pytania numer 40?', 1, 2, 1),
(41, 'Pytanie 41', 'Treść pytania numer 41?', 2, 3, 1),
(42, 'Pytanie 42', 'Treść pytania numer 42?', 1, 1, 1),
(43, 'Pytanie 43', 'Treść pytania numer 43?', 2, 2, 1),
(44, 'Pytanie 44', 'Treść pytania numer 44?', 1, 3, 1),
(45, 'Pytanie 45', 'Treść pytania numer 45?', 2, 1, 1),
(46, 'Pytanie 46', 'Treść pytania numer 46?', 1, 2, 1),
(47, 'Pytanie 47', 'Treść pytania numer 47?', 2, 3, 1),
(48, 'Pytanie 48', 'Treść pytania numer 48?', 1, 1, 1),
(49, 'Pytanie 49', 'Treść pytania numer 49?', 2, 2, 1),
(50, 'Pytanie 50', 'Treść pytania numer 50?', 1, 3, 1),
(51, 'Pytanie 51', 'Treść pytania numer 51?', 2, 1, 1),
(52, 'Pytanie 52', 'Treść pytania numer 52?', 1, 2, 1),
(53, 'Pytanie 53', 'Treść pytania numer 53?', 2, 3, 1),
(54, 'Pytanie 54', 'Treść pytania numer 54?', 1, 1, 1),
(55, 'Pytanie 55', 'Treść pytania numer 55?', 2, 2, 1),
(56, 'Pytanie 56', 'Treść pytania numer 56?', 1, 3, 1),
(57, 'Pytanie 57', 'Treść pytania numer 57?', 2, 1, 1),
(58, 'Pytanie 58', 'Treść pytania numer 58?', 1, 2, 1),
(59, 'Pytanie 59', 'Treść pytania numer 59?', 2, 3, 1),
(60, 'Pytanie 60', 'Treść pytania numer 60?', 1, 1, 1),
(61, 'Pytanie 61', 'Treść pytania numer 61?', 2, 2, 1),
(62, 'Pytanie 62', 'Treść pytania numer 62?', 1, 3, 1),
(63, 'Pytanie 63', 'Treść pytania numer 63?', 2, 1, 1),
(64, 'Pytanie 64', 'Treść pytania numer 64?', 1, 2, 1),
(65, 'Pytanie 65', 'Treść pytania numer 65?', 2, 3, 1),
(66, 'Pytanie 66', 'Treść pytania numer 66?', 1, 1, 1),
(67, 'Pytanie 67', 'Treść pytania numer 67?', 2, 2, 1),
(68, 'Pytanie 68', 'Treść pytania numer 68?', 1, 3, 1),
(69, 'Pytanie 69', 'Treść pytania numer 69?', 2, 1, 1),
(70, 'Pytanie 70', 'Treść pytania numer 70?', 1, 2, 1),
(71, 'Pytanie 71', 'Treść pytania numer 71?', 2, 3, 1),
(72, 'Pytanie 72', 'Treść pytania numer 72?', 1, 1, 1),
(73, 'Pytanie 73', 'Treść pytania numer 73?', 2, 2, 1),
(74, 'Pytanie 74', 'Treść pytania numer 74?', 1, 3, 1),
(75, 'Pytanie 75', 'Treść pytania numer 75?', 2, 1, 1),
(76, 'Pytanie 76', 'Treść pytania numer 76?', 1, 2, 1),
(77, 'Pytanie 77', 'Treść pytania numer 77?', 2, 3, 1),
(78, 'Pytanie 78', 'Treść pytania numer 78?', 1, 1, 1),
(79, 'Pytanie 79', 'Treść pytania numer 79?', 2, 2, 1),
(80, 'Pytanie 80', 'Treść pytania numer 80?', 1, 3, 1),
(81, 'Pytanie 81', 'Treść pytania numer 81?', 2, 1, 1),
(82, 'Pytanie 82', 'Treść pytania numer 82?', 1, 2, 1),
(83, 'Pytanie 83', 'Treść pytania numer 83?', 2, 3, 1),
(84, 'Pytanie 84', 'Treść pytania numer 84?', 1, 1, 1),
(85, 'Pytanie 85', 'Treść pytania numer 85?', 2, 2, 1),
(86, 'Pytanie 86', 'Treść pytania numer 86?', 1, 3, 1),
(87, 'Pytanie 87', 'Treść pytania numer 87?', 2, 1, 1),
(88, 'Pytanie 88', 'Treść pytania numer 88?', 1, 2, 1),
(89, 'Pytanie 89', 'Treść pytania numer 89?', 2, 3, 1),
(90, 'Pytanie 90', 'Treść pytania numer 90?', 1, 1, 1),
(91, 'Pytanie 91', 'Treść pytania numer 91?', 2, 2, 1),
(92, 'Pytanie 92', 'Treść pytania numer 92?', 1, 3, 1),
(93, 'Pytanie 93', 'Treść pytania numer 93?', 2, 1, 1),
(94, 'Pytanie 94', 'Treść pytania numer 94?', 1, 2, 1),
(95, 'Pytanie 95', 'Treść pytania numer 95?', 2, 3, 1),
(96, 'Pytanie 96', 'Treść pytania numer 96?', 1, 1, 1),
(97, 'Pytanie 97', 'Treść pytania numer 97?', 2, 2, 1),
(98, 'Pytanie 98', 'Treść pytania numer 98?', 1, 3, 1),
(99, 'Pytanie 99', 'Treść pytania numer 99?', 2, 1, 1),
(100, 'Pytanie 100', 'Treść pytania numer 100?', 1, 2, 1);

--
-- Zrzut danych tabeli `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `content`, `correct`) VALUES
(1, 1, 'Odpowiedź nr 1 do pytania o numerze ID 1', 1),
(2, 1, 'Odpowiedź nr 2 do pytania o numerze ID 1', 0),
(3, 1, 'Odpowiedź nr 3 do pytania o numerze ID 1', 1),
(4, 1, 'Odpowiedź nr 4 do pytania o numerze ID 1', 0),
(5, 2, 'Odpowiedź nr 1 do pytania o numerze ID 2', 1),
(6, 2, 'Odpowiedź nr 2 do pytania o numerze ID 2', 0),
(7, 2, 'Odpowiedź nr 3 do pytania o numerze ID 2', 1),
(8, 2, 'Odpowiedź nr 4 do pytania o numerze ID 2', 0),
(9, 3, 'Odpowiedź nr 1 do pytania o numerze ID 3', 1),
(10, 3, 'Odpowiedź nr 2 do pytania o numerze ID 3', 0),
(11, 3, 'Odpowiedź nr 3 do pytania o numerze ID 3', 1),
(12, 3, 'Odpowiedź nr 4 do pytania o numerze ID 3', 0),
(13, 4, 'Odpowiedź nr 1 do pytania o numerze ID 4', 1),
(14, 4, 'Odpowiedź nr 2 do pytania o numerze ID 4', 0),
(15, 4, 'Odpowiedź nr 3 do pytania o numerze ID 4', 1),
(16, 4, 'Odpowiedź nr 4 do pytania o numerze ID 4', 0),
(17, 5, 'Odpowiedź nr 1 do pytania o numerze ID 5', 1),
(18, 5, 'Odpowiedź nr 2 do pytania o numerze ID 5', 0),
(19, 5, 'Odpowiedź nr 3 do pytania o numerze ID 5', 1),
(20, 5, 'Odpowiedź nr 4 do pytania o numerze ID 5', 0),
(21, 6, 'Odpowiedź nr 1 do pytania o numerze ID 6', 1),
(22, 6, 'Odpowiedź nr 2 do pytania o numerze ID 6', 0),
(23, 6, 'Odpowiedź nr 3 do pytania o numerze ID 6', 1),
(24, 6, 'Odpowiedź nr 4 do pytania o numerze ID 6', 0),
(25, 7, 'Odpowiedź nr 1 do pytania o numerze ID 7', 1),
(26, 7, 'Odpowiedź nr 2 do pytania o numerze ID 7', 0),
(27, 7, 'Odpowiedź nr 3 do pytania o numerze ID 7', 1),
(28, 7, 'Odpowiedź nr 4 do pytania o numerze ID 7', 0),
(29, 8, 'Odpowiedź nr 1 do pytania o numerze ID 8', 1),
(30, 8, 'Odpowiedź nr 2 do pytania o numerze ID 8', 0),
(31, 8, 'Odpowiedź nr 3 do pytania o numerze ID 8', 1),
(32, 8, 'Odpowiedź nr 4 do pytania o numerze ID 8', 0),
(33, 9, 'Odpowiedź nr 1 do pytania o numerze ID 9', 1),
(34, 9, 'Odpowiedź nr 2 do pytania o numerze ID 9', 0),
(35, 9, 'Odpowiedź nr 3 do pytania o numerze ID 9', 1),
(36, 9, 'Odpowiedź nr 4 do pytania o numerze ID 9', 0),
(37, 10, 'Odpowiedź nr 1 do pytania o numerze ID 10', 1),
(38, 10, 'Odpowiedź nr 2 do pytania o numerze ID 10', 0),
(39, 10, 'Odpowiedź nr 3 do pytania o numerze ID 10', 1),
(40, 10, 'Odpowiedź nr 4 do pytania o numerze ID 10', 0),
(41, 11, 'Odpowiedź nr 1 do pytania o numerze ID 11', 1),
(42, 11, 'Odpowiedź nr 2 do pytania o numerze ID 11', 0),
(43, 11, 'Odpowiedź nr 3 do pytania o numerze ID 11', 1),
(44, 11, 'Odpowiedź nr 4 do pytania o numerze ID 11', 0),
(45, 12, 'Odpowiedź nr 1 do pytania o numerze ID 12', 1),
(46, 12, 'Odpowiedź nr 2 do pytania o numerze ID 12', 0),
(47, 12, 'Odpowiedź nr 3 do pytania o numerze ID 12', 1),
(48, 12, 'Odpowiedź nr 4 do pytania o numerze ID 12', 0),
(49, 13, 'Odpowiedź nr 1 do pytania o numerze ID 13', 1),
(50, 13, 'Odpowiedź nr 2 do pytania o numerze ID 13', 0),
(51, 13, 'Odpowiedź nr 3 do pytania o numerze ID 13', 1),
(52, 13, 'Odpowiedź nr 4 do pytania o numerze ID 13', 0),
(53, 14, 'Odpowiedź nr 1 do pytania o numerze ID 14', 1),
(54, 14, 'Odpowiedź nr 2 do pytania o numerze ID 14', 0),
(55, 14, 'Odpowiedź nr 3 do pytania o numerze ID 14', 1),
(56, 14, 'Odpowiedź nr 4 do pytania o numerze ID 14', 0),
(57, 15, 'Odpowiedź nr 1 do pytania o numerze ID 15', 1),
(58, 15, 'Odpowiedź nr 2 do pytania o numerze ID 15', 0),
(59, 15, 'Odpowiedź nr 3 do pytania o numerze ID 15', 1),
(60, 15, 'Odpowiedź nr 4 do pytania o numerze ID 15', 0),
(61, 16, 'Odpowiedź nr 1 do pytania o numerze ID 16', 1),
(62, 16, 'Odpowiedź nr 2 do pytania o numerze ID 16', 0),
(63, 16, 'Odpowiedź nr 3 do pytania o numerze ID 16', 1),
(64, 16, 'Odpowiedź nr 4 do pytania o numerze ID 16', 0),
(65, 17, 'Odpowiedź nr 1 do pytania o numerze ID 17', 1),
(66, 17, 'Odpowiedź nr 2 do pytania o numerze ID 17', 0),
(67, 17, 'Odpowiedź nr 3 do pytania o numerze ID 17', 1),
(68, 17, 'Odpowiedź nr 4 do pytania o numerze ID 17', 0),
(69, 18, 'Odpowiedź nr 1 do pytania o numerze ID 18', 1),
(70, 18, 'Odpowiedź nr 2 do pytania o numerze ID 18', 0),
(71, 18, 'Odpowiedź nr 3 do pytania o numerze ID 18', 1),
(72, 18, 'Odpowiedź nr 4 do pytania o numerze ID 18', 0),
(73, 19, 'Odpowiedź nr 1 do pytania o numerze ID 19', 1),
(74, 19, 'Odpowiedź nr 2 do pytania o numerze ID 19', 0),
(75, 19, 'Odpowiedź nr 3 do pytania o numerze ID 19', 1),
(76, 19, 'Odpowiedź nr 4 do pytania o numerze ID 19', 0),
(77, 20, 'Odpowiedź nr 1 do pytania o numerze ID 20', 1),
(78, 20, 'Odpowiedź nr 2 do pytania o numerze ID 20', 0),
(79, 20, 'Odpowiedź nr 3 do pytania o numerze ID 20', 1),
(80, 20, 'Odpowiedź nr 4 do pytania o numerze ID 20', 0),
(81, 21, 'Odpowiedź nr 1 do pytania o numerze ID 21', 1),
(82, 21, 'Odpowiedź nr 2 do pytania o numerze ID 21', 0),
(83, 21, 'Odpowiedź nr 3 do pytania o numerze ID 21', 1),
(84, 21, 'Odpowiedź nr 4 do pytania o numerze ID 21', 0),
(85, 22, 'Odpowiedź nr 1 do pytania o numerze ID 22', 1),
(86, 22, 'Odpowiedź nr 2 do pytania o numerze ID 22', 0),
(87, 22, 'Odpowiedź nr 3 do pytania o numerze ID 22', 1),
(88, 22, 'Odpowiedź nr 4 do pytania o numerze ID 22', 0),
(89, 23, 'Odpowiedź nr 1 do pytania o numerze ID 23', 1),
(90, 23, 'Odpowiedź nr 2 do pytania o numerze ID 23', 0),
(91, 23, 'Odpowiedź nr 3 do pytania o numerze ID 23', 1),
(92, 23, 'Odpowiedź nr 4 do pytania o numerze ID 23', 0),
(93, 24, 'Odpowiedź nr 1 do pytania o numerze ID 24', 1),
(94, 24, 'Odpowiedź nr 2 do pytania o numerze ID 24', 0),
(95, 24, 'Odpowiedź nr 3 do pytania o numerze ID 24', 1),
(96, 24, 'Odpowiedź nr 4 do pytania o numerze ID 24', 0),
(97, 25, 'Odpowiedź nr 1 do pytania o numerze ID 25', 1),
(98, 25, 'Odpowiedź nr 2 do pytania o numerze ID 25', 0),
(99, 25, 'Odpowiedź nr 3 do pytania o numerze ID 25', 1),
(100, 25, 'Odpowiedź nr 4 do pytania o numerze ID 25', 0),
(101, 26, 'Odpowiedź nr 1 do pytania o numerze ID 26', 1),
(102, 26, 'Odpowiedź nr 2 do pytania o numerze ID 26', 0),
(103, 26, 'Odpowiedź nr 3 do pytania o numerze ID 26', 1),
(104, 26, 'Odpowiedź nr 4 do pytania o numerze ID 26', 0),
(105, 27, 'Odpowiedź nr 1 do pytania o numerze ID 27', 1),
(106, 27, 'Odpowiedź nr 2 do pytania o numerze ID 27', 0),
(107, 27, 'Odpowiedź nr 3 do pytania o numerze ID 27', 1),
(108, 27, 'Odpowiedź nr 4 do pytania o numerze ID 27', 0),
(109, 28, 'Odpowiedź nr 1 do pytania o numerze ID 28', 1),
(110, 28, 'Odpowiedź nr 2 do pytania o numerze ID 28', 0),
(111, 28, 'Odpowiedź nr 3 do pytania o numerze ID 28', 1),
(112, 28, 'Odpowiedź nr 4 do pytania o numerze ID 28', 0),
(113, 29, 'Odpowiedź nr 1 do pytania o numerze ID 29', 1),
(114, 29, 'Odpowiedź nr 2 do pytania o numerze ID 29', 0),
(115, 29, 'Odpowiedź nr 3 do pytania o numerze ID 29', 1),
(116, 29, 'Odpowiedź nr 4 do pytania o numerze ID 29', 0),
(117, 30, 'Odpowiedź nr 1 do pytania o numerze ID 30', 1),
(118, 30, 'Odpowiedź nr 2 do pytania o numerze ID 30', 0),
(119, 30, 'Odpowiedź nr 3 do pytania o numerze ID 30', 1),
(120, 30, 'Odpowiedź nr 4 do pytania o numerze ID 30', 0),
(121, 31, 'Odpowiedź nr 1 do pytania o numerze ID 31', 1),
(122, 31, 'Odpowiedź nr 2 do pytania o numerze ID 31', 0),
(123, 31, 'Odpowiedź nr 3 do pytania o numerze ID 31', 1),
(124, 31, 'Odpowiedź nr 4 do pytania o numerze ID 31', 0),
(125, 32, 'Odpowiedź nr 1 do pytania o numerze ID 32', 1),
(126, 32, 'Odpowiedź nr 2 do pytania o numerze ID 32', 0),
(127, 32, 'Odpowiedź nr 3 do pytania o numerze ID 32', 1),
(128, 32, 'Odpowiedź nr 4 do pytania o numerze ID 32', 0),
(129, 33, 'Odpowiedź nr 1 do pytania o numerze ID 33', 1),
(130, 33, 'Odpowiedź nr 2 do pytania o numerze ID 33', 0),
(131, 33, 'Odpowiedź nr 3 do pytania o numerze ID 33', 1),
(132, 33, 'Odpowiedź nr 4 do pytania o numerze ID 33', 0),
(133, 34, 'Odpowiedź nr 1 do pytania o numerze ID 34', 1),
(134, 34, 'Odpowiedź nr 2 do pytania o numerze ID 34', 0),
(135, 34, 'Odpowiedź nr 3 do pytania o numerze ID 34', 1),
(136, 34, 'Odpowiedź nr 4 do pytania o numerze ID 34', 0),
(137, 35, 'Odpowiedź nr 1 do pytania o numerze ID 35', 1),
(138, 35, 'Odpowiedź nr 2 do pytania o numerze ID 35', 0),
(139, 35, 'Odpowiedź nr 3 do pytania o numerze ID 35', 1),
(140, 35, 'Odpowiedź nr 4 do pytania o numerze ID 35', 0),
(141, 36, 'Odpowiedź nr 1 do pytania o numerze ID 36', 1),
(142, 36, 'Odpowiedź nr 2 do pytania o numerze ID 36', 0),
(143, 36, 'Odpowiedź nr 3 do pytania o numerze ID 36', 1),
(144, 36, 'Odpowiedź nr 4 do pytania o numerze ID 36', 0),
(145, 37, 'Odpowiedź nr 1 do pytania o numerze ID 37', 1),
(146, 37, 'Odpowiedź nr 2 do pytania o numerze ID 37', 0),
(147, 37, 'Odpowiedź nr 3 do pytania o numerze ID 37', 1),
(148, 37, 'Odpowiedź nr 4 do pytania o numerze ID 37', 0),
(149, 38, 'Odpowiedź nr 1 do pytania o numerze ID 38', 1),
(150, 38, 'Odpowiedź nr 2 do pytania o numerze ID 38', 0),
(151, 38, 'Odpowiedź nr 3 do pytania o numerze ID 38', 1),
(152, 38, 'Odpowiedź nr 4 do pytania o numerze ID 38', 0),
(153, 39, 'Odpowiedź nr 1 do pytania o numerze ID 39', 1),
(154, 39, 'Odpowiedź nr 2 do pytania o numerze ID 39', 0),
(155, 39, 'Odpowiedź nr 3 do pytania o numerze ID 39', 1),
(156, 39, 'Odpowiedź nr 4 do pytania o numerze ID 39', 0),
(157, 40, 'Odpowiedź nr 1 do pytania o numerze ID 40', 1),
(158, 40, 'Odpowiedź nr 2 do pytania o numerze ID 40', 0),
(159, 40, 'Odpowiedź nr 3 do pytania o numerze ID 40', 1),
(160, 40, 'Odpowiedź nr 4 do pytania o numerze ID 40', 0),
(161, 41, 'Odpowiedź nr 1 do pytania o numerze ID 41', 1),
(162, 41, 'Odpowiedź nr 2 do pytania o numerze ID 41', 0),
(163, 41, 'Odpowiedź nr 3 do pytania o numerze ID 41', 1),
(164, 41, 'Odpowiedź nr 4 do pytania o numerze ID 41', 0),
(165, 42, 'Odpowiedź nr 1 do pytania o numerze ID 42', 1),
(166, 42, 'Odpowiedź nr 2 do pytania o numerze ID 42', 0),
(167, 42, 'Odpowiedź nr 3 do pytania o numerze ID 42', 1),
(168, 42, 'Odpowiedź nr 4 do pytania o numerze ID 42', 0),
(169, 43, 'Odpowiedź nr 1 do pytania o numerze ID 43', 1),
(170, 43, 'Odpowiedź nr 2 do pytania o numerze ID 43', 0),
(171, 43, 'Odpowiedź nr 3 do pytania o numerze ID 43', 1),
(172, 43, 'Odpowiedź nr 4 do pytania o numerze ID 43', 0),
(173, 44, 'Odpowiedź nr 1 do pytania o numerze ID 44', 1),
(174, 44, 'Odpowiedź nr 2 do pytania o numerze ID 44', 0),
(175, 44, 'Odpowiedź nr 3 do pytania o numerze ID 44', 1),
(176, 44, 'Odpowiedź nr 4 do pytania o numerze ID 44', 0),
(177, 45, 'Odpowiedź nr 1 do pytania o numerze ID 45', 1),
(178, 45, 'Odpowiedź nr 2 do pytania o numerze ID 45', 0),
(179, 45, 'Odpowiedź nr 3 do pytania o numerze ID 45', 1),
(180, 45, 'Odpowiedź nr 4 do pytania o numerze ID 45', 0),
(181, 46, 'Odpowiedź nr 1 do pytania o numerze ID 46', 1),
(182, 46, 'Odpowiedź nr 2 do pytania o numerze ID 46', 0),
(183, 46, 'Odpowiedź nr 3 do pytania o numerze ID 46', 1),
(184, 46, 'Odpowiedź nr 4 do pytania o numerze ID 46', 0),
(185, 47, 'Odpowiedź nr 1 do pytania o numerze ID 47', 1),
(186, 47, 'Odpowiedź nr 2 do pytania o numerze ID 47', 0),
(187, 47, 'Odpowiedź nr 3 do pytania o numerze ID 47', 1),
(188, 47, 'Odpowiedź nr 4 do pytania o numerze ID 47', 0),
(189, 48, 'Odpowiedź nr 1 do pytania o numerze ID 48', 1),
(190, 48, 'Odpowiedź nr 2 do pytania o numerze ID 48', 0),
(191, 48, 'Odpowiedź nr 3 do pytania o numerze ID 48', 1),
(192, 48, 'Odpowiedź nr 4 do pytania o numerze ID 48', 0),
(193, 49, 'Odpowiedź nr 1 do pytania o numerze ID 49', 1),
(194, 49, 'Odpowiedź nr 2 do pytania o numerze ID 49', 0),
(195, 49, 'Odpowiedź nr 3 do pytania o numerze ID 49', 1),
(196, 49, 'Odpowiedź nr 4 do pytania o numerze ID 49', 0),
(197, 50, 'Odpowiedź nr 1 do pytania o numerze ID 50', 1),
(198, 50, 'Odpowiedź nr 2 do pytania o numerze ID 50', 0),
(199, 50, 'Odpowiedź nr 3 do pytania o numerze ID 50', 1),
(200, 50, 'Odpowiedź nr 4 do pytania o numerze ID 50', 0),
(201, 51, 'Odpowiedź nr 1 do pytania o numerze ID 51', 1),
(202, 51, 'Odpowiedź nr 2 do pytania o numerze ID 51', 0),
(203, 51, 'Odpowiedź nr 3 do pytania o numerze ID 51', 1),
(204, 51, 'Odpowiedź nr 4 do pytania o numerze ID 51', 0),
(205, 52, 'Odpowiedź nr 1 do pytania o numerze ID 52', 1),
(206, 52, 'Odpowiedź nr 2 do pytania o numerze ID 52', 0),
(207, 52, 'Odpowiedź nr 3 do pytania o numerze ID 52', 1),
(208, 52, 'Odpowiedź nr 4 do pytania o numerze ID 52', 0),
(209, 53, 'Odpowiedź nr 1 do pytania o numerze ID 53', 1),
(210, 53, 'Odpowiedź nr 2 do pytania o numerze ID 53', 0),
(211, 53, 'Odpowiedź nr 3 do pytania o numerze ID 53', 1),
(212, 53, 'Odpowiedź nr 4 do pytania o numerze ID 53', 0),
(213, 54, 'Odpowiedź nr 1 do pytania o numerze ID 54', 1),
(214, 54, 'Odpowiedź nr 2 do pytania o numerze ID 54', 0),
(215, 54, 'Odpowiedź nr 3 do pytania o numerze ID 54', 1),
(216, 54, 'Odpowiedź nr 4 do pytania o numerze ID 54', 0),
(217, 55, 'Odpowiedź nr 1 do pytania o numerze ID 55', 1),
(218, 55, 'Odpowiedź nr 2 do pytania o numerze ID 55', 0),
(219, 55, 'Odpowiedź nr 3 do pytania o numerze ID 55', 1),
(220, 55, 'Odpowiedź nr 4 do pytania o numerze ID 55', 0),
(221, 56, 'Odpowiedź nr 1 do pytania o numerze ID 56', 1),
(222, 56, 'Odpowiedź nr 2 do pytania o numerze ID 56', 0),
(223, 56, 'Odpowiedź nr 3 do pytania o numerze ID 56', 1),
(224, 56, 'Odpowiedź nr 4 do pytania o numerze ID 56', 0),
(225, 57, 'Odpowiedź nr 1 do pytania o numerze ID 57', 1),
(226, 57, 'Odpowiedź nr 2 do pytania o numerze ID 57', 0),
(227, 57, 'Odpowiedź nr 3 do pytania o numerze ID 57', 1),
(228, 57, 'Odpowiedź nr 4 do pytania o numerze ID 57', 0),
(229, 58, 'Odpowiedź nr 1 do pytania o numerze ID 58', 1),
(230, 58, 'Odpowiedź nr 2 do pytania o numerze ID 58', 0),
(231, 58, 'Odpowiedź nr 3 do pytania o numerze ID 58', 1),
(232, 58, 'Odpowiedź nr 4 do pytania o numerze ID 58', 0),
(233, 59, 'Odpowiedź nr 1 do pytania o numerze ID 59', 1),
(234, 59, 'Odpowiedź nr 2 do pytania o numerze ID 59', 0),
(235, 59, 'Odpowiedź nr 3 do pytania o numerze ID 59', 1),
(236, 59, 'Odpowiedź nr 4 do pytania o numerze ID 59', 0),
(237, 60, 'Odpowiedź nr 1 do pytania o numerze ID 60', 1),
(238, 60, 'Odpowiedź nr 2 do pytania o numerze ID 60', 0),
(239, 60, 'Odpowiedź nr 3 do pytania o numerze ID 60', 1),
(240, 60, 'Odpowiedź nr 4 do pytania o numerze ID 60', 0),
(241, 61, 'Odpowiedź nr 1 do pytania o numerze ID 61', 1),
(242, 61, 'Odpowiedź nr 2 do pytania o numerze ID 61', 0),
(243, 61, 'Odpowiedź nr 3 do pytania o numerze ID 61', 1),
(244, 61, 'Odpowiedź nr 4 do pytania o numerze ID 61', 0),
(245, 62, 'Odpowiedź nr 1 do pytania o numerze ID 62', 1),
(246, 62, 'Odpowiedź nr 2 do pytania o numerze ID 62', 0),
(247, 62, 'Odpowiedź nr 3 do pytania o numerze ID 62', 1),
(248, 62, 'Odpowiedź nr 4 do pytania o numerze ID 62', 0),
(249, 63, 'Odpowiedź nr 1 do pytania o numerze ID 63', 1),
(250, 63, 'Odpowiedź nr 2 do pytania o numerze ID 63', 0),
(251, 63, 'Odpowiedź nr 3 do pytania o numerze ID 63', 1),
(252, 63, 'Odpowiedź nr 4 do pytania o numerze ID 63', 0),
(253, 64, 'Odpowiedź nr 1 do pytania o numerze ID 64', 1),
(254, 64, 'Odpowiedź nr 2 do pytania o numerze ID 64', 0),
(255, 64, 'Odpowiedź nr 3 do pytania o numerze ID 64', 1),
(256, 64, 'Odpowiedź nr 4 do pytania o numerze ID 64', 0),
(257, 65, 'Odpowiedź nr 1 do pytania o numerze ID 65', 1),
(258, 65, 'Odpowiedź nr 2 do pytania o numerze ID 65', 0),
(259, 65, 'Odpowiedź nr 3 do pytania o numerze ID 65', 1),
(260, 65, 'Odpowiedź nr 4 do pytania o numerze ID 65', 0),
(261, 66, 'Odpowiedź nr 1 do pytania o numerze ID 66', 1),
(262, 66, 'Odpowiedź nr 2 do pytania o numerze ID 66', 0),
(263, 66, 'Odpowiedź nr 3 do pytania o numerze ID 66', 1),
(264, 66, 'Odpowiedź nr 4 do pytania o numerze ID 66', 0),
(265, 67, 'Odpowiedź nr 1 do pytania o numerze ID 67', 1),
(266, 67, 'Odpowiedź nr 2 do pytania o numerze ID 67', 0),
(267, 67, 'Odpowiedź nr 3 do pytania o numerze ID 67', 1),
(268, 67, 'Odpowiedź nr 4 do pytania o numerze ID 67', 0),
(269, 68, 'Odpowiedź nr 1 do pytania o numerze ID 68', 1),
(270, 68, 'Odpowiedź nr 2 do pytania o numerze ID 68', 0),
(271, 68, 'Odpowiedź nr 3 do pytania o numerze ID 68', 1),
(272, 68, 'Odpowiedź nr 4 do pytania o numerze ID 68', 0),
(273, 69, 'Odpowiedź nr 1 do pytania o numerze ID 69', 1),
(274, 69, 'Odpowiedź nr 2 do pytania o numerze ID 69', 0),
(275, 69, 'Odpowiedź nr 3 do pytania o numerze ID 69', 1),
(276, 69, 'Odpowiedź nr 4 do pytania o numerze ID 69', 0),
(277, 70, 'Odpowiedź nr 1 do pytania o numerze ID 70', 1),
(278, 70, 'Odpowiedź nr 2 do pytania o numerze ID 70', 0),
(279, 70, 'Odpowiedź nr 3 do pytania o numerze ID 70', 1),
(280, 70, 'Odpowiedź nr 4 do pytania o numerze ID 70', 0),
(281, 71, 'Odpowiedź nr 1 do pytania o numerze ID 71', 1),
(282, 71, 'Odpowiedź nr 2 do pytania o numerze ID 71', 0),
(283, 71, 'Odpowiedź nr 3 do pytania o numerze ID 71', 1),
(284, 71, 'Odpowiedź nr 4 do pytania o numerze ID 71', 0),
(285, 72, 'Odpowiedź nr 1 do pytania o numerze ID 72', 1),
(286, 72, 'Odpowiedź nr 2 do pytania o numerze ID 72', 0),
(287, 72, 'Odpowiedź nr 3 do pytania o numerze ID 72', 1),
(288, 72, 'Odpowiedź nr 4 do pytania o numerze ID 72', 0),
(289, 73, 'Odpowiedź nr 1 do pytania o numerze ID 73', 1),
(290, 73, 'Odpowiedź nr 2 do pytania o numerze ID 73', 0),
(291, 73, 'Odpowiedź nr 3 do pytania o numerze ID 73', 1),
(292, 73, 'Odpowiedź nr 4 do pytania o numerze ID 73', 0),
(293, 74, 'Odpowiedź nr 1 do pytania o numerze ID 74', 1),
(294, 74, 'Odpowiedź nr 2 do pytania o numerze ID 74', 0),
(295, 74, 'Odpowiedź nr 3 do pytania o numerze ID 74', 1),
(296, 74, 'Odpowiedź nr 4 do pytania o numerze ID 74', 0),
(297, 75, 'Odpowiedź nr 1 do pytania o numerze ID 75', 1),
(298, 75, 'Odpowiedź nr 2 do pytania o numerze ID 75', 0),
(299, 75, 'Odpowiedź nr 3 do pytania o numerze ID 75', 1),
(300, 75, 'Odpowiedź nr 4 do pytania o numerze ID 75', 0),
(301, 76, 'Odpowiedź nr 1 do pytania o numerze ID 76', 1),
(302, 76, 'Odpowiedź nr 2 do pytania o numerze ID 76', 0),
(303, 76, 'Odpowiedź nr 3 do pytania o numerze ID 76', 1),
(304, 76, 'Odpowiedź nr 4 do pytania o numerze ID 76', 0),
(305, 77, 'Odpowiedź nr 1 do pytania o numerze ID 77', 1),
(306, 77, 'Odpowiedź nr 2 do pytania o numerze ID 77', 0),
(307, 77, 'Odpowiedź nr 3 do pytania o numerze ID 77', 1),
(308, 77, 'Odpowiedź nr 4 do pytania o numerze ID 77', 0),
(309, 78, 'Odpowiedź nr 1 do pytania o numerze ID 78', 1),
(310, 78, 'Odpowiedź nr 2 do pytania o numerze ID 78', 0),
(311, 78, 'Odpowiedź nr 3 do pytania o numerze ID 78', 1),
(312, 78, 'Odpowiedź nr 4 do pytania o numerze ID 78', 0),
(313, 79, 'Odpowiedź nr 1 do pytania o numerze ID 79', 1),
(314, 79, 'Odpowiedź nr 2 do pytania o numerze ID 79', 0),
(315, 79, 'Odpowiedź nr 3 do pytania o numerze ID 79', 1),
(316, 79, 'Odpowiedź nr 4 do pytania o numerze ID 79', 0),
(317, 80, 'Odpowiedź nr 1 do pytania o numerze ID 80', 1),
(318, 80, 'Odpowiedź nr 2 do pytania o numerze ID 80', 0),
(319, 80, 'Odpowiedź nr 3 do pytania o numerze ID 80', 1),
(320, 80, 'Odpowiedź nr 4 do pytania o numerze ID 80', 0),
(321, 81, 'Odpowiedź nr 1 do pytania o numerze ID 81', 1),
(322, 81, 'Odpowiedź nr 2 do pytania o numerze ID 81', 0),
(323, 81, 'Odpowiedź nr 3 do pytania o numerze ID 81', 1),
(324, 81, 'Odpowiedź nr 4 do pytania o numerze ID 81', 0),
(325, 82, 'Odpowiedź nr 1 do pytania o numerze ID 82', 1),
(326, 82, 'Odpowiedź nr 2 do pytania o numerze ID 82', 0),
(327, 82, 'Odpowiedź nr 3 do pytania o numerze ID 82', 1),
(328, 82, 'Odpowiedź nr 4 do pytania o numerze ID 82', 0),
(329, 83, 'Odpowiedź nr 1 do pytania o numerze ID 83', 1),
(330, 83, 'Odpowiedź nr 2 do pytania o numerze ID 83', 0),
(331, 83, 'Odpowiedź nr 3 do pytania o numerze ID 83', 1),
(332, 83, 'Odpowiedź nr 4 do pytania o numerze ID 83', 0),
(333, 84, 'Odpowiedź nr 1 do pytania o numerze ID 84', 1),
(334, 84, 'Odpowiedź nr 2 do pytania o numerze ID 84', 0),
(335, 84, 'Odpowiedź nr 3 do pytania o numerze ID 84', 1),
(336, 84, 'Odpowiedź nr 4 do pytania o numerze ID 84', 0),
(337, 85, 'Odpowiedź nr 1 do pytania o numerze ID 85', 1),
(338, 85, 'Odpowiedź nr 2 do pytania o numerze ID 85', 0),
(339, 85, 'Odpowiedź nr 3 do pytania o numerze ID 85', 1),
(340, 85, 'Odpowiedź nr 4 do pytania o numerze ID 85', 0),
(341, 86, 'Odpowiedź nr 1 do pytania o numerze ID 86', 1),
(342, 86, 'Odpowiedź nr 2 do pytania o numerze ID 86', 0),
(343, 86, 'Odpowiedź nr 3 do pytania o numerze ID 86', 1),
(344, 86, 'Odpowiedź nr 4 do pytania o numerze ID 86', 0),
(345, 87, 'Odpowiedź nr 1 do pytania o numerze ID 87', 1),
(346, 87, 'Odpowiedź nr 2 do pytania o numerze ID 87', 0),
(347, 87, 'Odpowiedź nr 3 do pytania o numerze ID 87', 1),
(348, 87, 'Odpowiedź nr 4 do pytania o numerze ID 87', 0),
(349, 88, 'Odpowiedź nr 1 do pytania o numerze ID 88', 1),
(350, 88, 'Odpowiedź nr 2 do pytania o numerze ID 88', 0),
(351, 88, 'Odpowiedź nr 3 do pytania o numerze ID 88', 1),
(352, 88, 'Odpowiedź nr 4 do pytania o numerze ID 88', 0),
(353, 89, 'Odpowiedź nr 1 do pytania o numerze ID 89', 1),
(354, 89, 'Odpowiedź nr 2 do pytania o numerze ID 89', 0),
(355, 89, 'Odpowiedź nr 3 do pytania o numerze ID 89', 1),
(356, 89, 'Odpowiedź nr 4 do pytania o numerze ID 89', 0),
(357, 90, 'Odpowiedź nr 1 do pytania o numerze ID 90', 1),
(358, 90, 'Odpowiedź nr 2 do pytania o numerze ID 90', 0),
(359, 90, 'Odpowiedź nr 3 do pytania o numerze ID 90', 1),
(360, 90, 'Odpowiedź nr 4 do pytania o numerze ID 90', 0),
(361, 91, 'Odpowiedź nr 1 do pytania o numerze ID 91', 1),
(362, 91, 'Odpowiedź nr 2 do pytania o numerze ID 91', 0),
(363, 91, 'Odpowiedź nr 3 do pytania o numerze ID 91', 1),
(364, 91, 'Odpowiedź nr 4 do pytania o numerze ID 91', 0),
(365, 92, 'Odpowiedź nr 1 do pytania o numerze ID 92', 1),
(366, 92, 'Odpowiedź nr 2 do pytania o numerze ID 92', 0),
(367, 92, 'Odpowiedź nr 3 do pytania o numerze ID 92', 1),
(368, 92, 'Odpowiedź nr 4 do pytania o numerze ID 92', 0),
(369, 93, 'Odpowiedź nr 1 do pytania o numerze ID 93', 1),
(370, 93, 'Odpowiedź nr 2 do pytania o numerze ID 93', 0),
(371, 93, 'Odpowiedź nr 3 do pytania o numerze ID 93', 1),
(372, 93, 'Odpowiedź nr 4 do pytania o numerze ID 93', 0),
(373, 94, 'Odpowiedź nr 1 do pytania o numerze ID 94', 1),
(374, 94, 'Odpowiedź nr 2 do pytania o numerze ID 94', 0),
(375, 94, 'Odpowiedź nr 3 do pytania o numerze ID 94', 1),
(376, 94, 'Odpowiedź nr 4 do pytania o numerze ID 94', 0),
(377, 95, 'Odpowiedź nr 1 do pytania o numerze ID 95', 1),
(378, 95, 'Odpowiedź nr 2 do pytania o numerze ID 95', 0),
(379, 95, 'Odpowiedź nr 3 do pytania o numerze ID 95', 1),
(380, 95, 'Odpowiedź nr 4 do pytania o numerze ID 95', 0),
(381, 96, 'Odpowiedź nr 1 do pytania o numerze ID 96', 1),
(382, 96, 'Odpowiedź nr 2 do pytania o numerze ID 96', 0),
(383, 96, 'Odpowiedź nr 3 do pytania o numerze ID 96', 1),
(384, 96, 'Odpowiedź nr 4 do pytania o numerze ID 96', 0),
(385, 97, 'Odpowiedź nr 1 do pytania o numerze ID 97', 1),
(386, 97, 'Odpowiedź nr 2 do pytania o numerze ID 97', 0),
(387, 97, 'Odpowiedź nr 3 do pytania o numerze ID 97', 1),
(388, 97, 'Odpowiedź nr 4 do pytania o numerze ID 97', 0),
(389, 98, 'Odpowiedź nr 1 do pytania o numerze ID 98', 1),
(390, 98, 'Odpowiedź nr 2 do pytania o numerze ID 98', 0),
(391, 98, 'Odpowiedź nr 3 do pytania o numerze ID 98', 1),
(392, 98, 'Odpowiedź nr 4 do pytania o numerze ID 98', 0),
(393, 99, 'Odpowiedź nr 1 do pytania o numerze ID 99', 1),
(394, 99, 'Odpowiedź nr 2 do pytania o numerze ID 99', 0),
(395, 99, 'Odpowiedź nr 3 do pytania o numerze ID 99', 1),
(396, 99, 'Odpowiedź nr 4 do pytania o numerze ID 99', 0),
(397, 100, 'Odpowiedź nr 1 do pytania o numerze ID 100', 1),
(398, 100, 'Odpowiedź nr 2 do pytania o numerze ID 100', 0),
(399, 100, 'Odpowiedź nr 3 do pytania o numerze ID 100', 1),
(400, 100, 'Odpowiedź nr 4 do pytania o numerze ID 100', 0);
