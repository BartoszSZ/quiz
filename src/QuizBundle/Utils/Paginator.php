<?php

namespace QuizBundle\Utils;

use QuizBundle\Utils\Dictionary;

class Paginator {

    /**
     * Templating service.
     *
     * @var Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $templating = null;

    /**
     * Paginator options.
     *
     * @var array
     */
    private $options = array();

    /**
     * Array of options used in view.
     *
     * @var array
     */
    private $viewVars = array();

    /**
     * Handles service parameters.
     *
     * @param Symfony\Bundle\TwigBundle\TwigEngine $templating
     * @param array $options
     */
    public function __construct($templating, $options)
    {
        $this->templating = $templating;
        $this->options = $options;
    }

    /**
     * Return limit of questions per page.
     * Named method instead of a magic caller, since QPP
     * is the only value needed outside Paginator object.
     *
     * @return integer
     * @throws \Exception
     */
    public function getMaxQuestionsPerPage()
    {
        if (!isset($this->options['questions_per_page'])) {
            throw new \Exception(Dictionary::getMessage(Dictionary::ERROR_PAGINATOR_QPP_NOT_SET));
        }

        if ($this->options['questions_per_page'] < 1) {
            throw new \Exception(Dictionary::getMessage(Dictionary::ERROR_PAGINATOR_MIN_QPP));
        }

        return $this->options['questions_per_page'];
    }

    /**
     * Paginates results and sets pagination options.
     *
     * @param integer $currentPage
     * @param integer $totalCount
     * @throws \Exception
     */
    public function paginate($currentPage, $totalCount)
    {
        $linksRange = $this->options['links_range'];
        $pagesCount = intval(ceil($totalCount / $this->getMaxQuestionsPerPage()));

        if ($pagesCount < 2 || $linksRange < 1) {
            // Disable pagination.
            $pages = array();
        } elseif ($pagesCount - $linksRange < 0) {
            // All pages in range.
            $pages = range(1, $pagesCount);
        } else {
            $delta = ceil($linksRange / 2);

            if ($currentPage - $delta < 0) {
                $pages = range(1, $linksRange);
            } elseif ($currentPage + $delta > $pagesCount) {
                $pages = range($pagesCount - $linksRange + 1, $pagesCount);
            } else {
                $first = $currentPage - $delta + 1;
                $pages = range($first, $first + $linksRange - 1);
            }
        }

        $this->viewVars['current_page'] = $currentPage;
        $this->viewVars['all_pages_count'] = $pagesCount;
        $this->viewVars['pages'] = $pages;
    }

    /**
     * Renders pagination HTML.
     *
     * @return string
     */
    public function render()
    {
        return $this->templating->render(
            $this->options['template'],
            array(
                'current_page' => $this->viewVars['current_page'],
                'all_pages_count' => $this->viewVars['all_pages_count'],
                'pages' => $this->viewVars['pages'],
                'route_parameter_name' => $this->options['route_parameter_page'],
            )
        );
    }
}
