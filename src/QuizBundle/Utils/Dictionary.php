<?php

namespace QuizBundle\Utils;

class Dictionary {

    const ERROR_GENERAL = 1;
    const ERROR_EMPTY_ID = 2;
    const ERROR_ID_TAKEN = 3;
    const ERROR_QUESTION_NOT_FOUND = 4;
    const ERROR_QUESTION_DEL_NOT_FOUND = 5;
    const ERROR_QUESTION_DEL_GENERAL = 6;
    const ERROR_PAGINATOR_MIN_QPP = 8;
    const ERROR_PAGINATOR_QPP_NOT_SET = 9;

    const SUCCESS_QUESTION_DEL = 7;
    const SUCCESS_QUESTION_SAVE = 10;

    const TYPE_FLASH_ERROR = 'error';
    const TYPE_FLASH_SUCCESS = 'success';

    private static $messages = array(
        self::ERROR_GENERAL => 'Wystąpił błąd. Prosze spróbować ponownie',
        self::ERROR_EMPTY_ID => 'Identyfikator nie może być pusty',
        self::ERROR_ID_TAKEN => 'Podany identyfikator jest zajęty, proszę podać inny',
        self::ERROR_QUESTION_NOT_FOUND => 'Nie ma pytania o takim ID',
        self::ERROR_QUESTION_DEL_NOT_FOUND => 'Nie można wykonać operacji usuwania ponieważ pytanie o numerze ID %d nie istnieje',
        self::ERROR_QUESTION_DEL_GENERAL => 'Wystąpił błąd, anulowano operacje usuwania',
        self::ERROR_PAGINATOR_MIN_QPP => 'Minimalna liczba pytań na stronę musi być większa od zera',
        self::ERROR_PAGINATOR_QPP_NOT_SET => 'Limit pytań na stronę nie został zdefiniowany w ustawieniach',

        self::SUCCESS_QUESTION_DEL => 'Usunięto pytanie o numerze ID %d',
        self::SUCCESS_QUESTION_SAVE => 'Zmiany zostały zapisane',
    );

    public static function getMessage($code, $values = array())
    {
        if (!isset(self::$messages[$code])) {
            throw new \Exception('W słowniku nie ma komunikatu pasującego do podanego kodu');
        }

        return vsprintf(self::$messages[$code], $values);
    }
}