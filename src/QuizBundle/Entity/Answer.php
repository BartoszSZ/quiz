<?php

namespace QuizBundle\Entity;

/**
 * Answer
 */
class Answer
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var boolean
     */
    private $correct = '0';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \QuizBundle\Entity\Question
     */
    private $question;


    /**
     * Set content
     *
     * @param string $content
     *
     * @return Answer
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set correct
     *
     * @param boolean $correct
     *
     * @return Answer
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;

        return $this;
    }

    /**
     * Get correct
     *
     * @return boolean
     */
    public function getCorrect()
    {
        return $this->correct;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param \QuizBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function setQuestion(\QuizBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \QuizBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}

