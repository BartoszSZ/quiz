<?php

namespace QuizBundle\Entity;

/**
 * Question
 */
class Question
{
    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $type = '1';

    /**
     * @var integer
     */
    private $level = '1';

    /**
     * @var boolean
     */
    private $status = '0';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return Question
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Question
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Question
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $answer->setQuestion($this);
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     */
    public function removeAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Check if there are at least two answers (min. 1 acive).
     *
     * @return boolean
     */
    public function isActivationLegal()
    {
        if ($this->getStatus() == false) {
            // Skip validation if question should not be active.
            return true;
        }

        $correctAnswers = 0;

        foreach ($this->getAnswers() as $answer) {
            $correctAnswers += $answer->getCorrect() == true ? 1 : 0;
        }

        return $this->getAnswers()->count() > 1
            && $correctAnswers > 0;
    }
}
