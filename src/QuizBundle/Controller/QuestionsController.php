<?php

namespace QuizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use QuizBundle\Entity\Question;
use QuizBundle\Form\Type\QuestionEditType;
use QuizBundle\Form\Type\QuestionAddType;
use QuizBundle\Utils\Dictionary;

class QuestionsController extends Controller
{
    /**
     * Entity manager.
     *
     * @var EntityManager object
     */
    private $em = null;

    /**
     * Entity repository.
     *
     * @var QuestionRepository object
     */
    private $repo = null;

    /**
     * Dashboard action.
     *
     * @param string $page
     * @param Request $request
     * @return Response
     */
    public function dashboardAction($page, Request $request)
    {
        $this->initDoctrine();

        $question = new Question();

        $form = $this->createForm(QuestionAddType::class, $question);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($question);
            $this->em->flush();

            return $this->redirectToRoute('_quiz_question_edit', array('id' => $question->getId()));
        }

        $paginator = $this->get('quiz.paginator');

        // Check current page number; get the nearest possible value if needed.
        $correctPage = $this->repo->getCorrectValueOfPage(
            (int)$page,
            $paginator->getMaxQuestionsPerPage()
        );

        // Redirect to correct page if needed.
        if ($correctPage != $page) {
            return $this->redirectToRoute('_quiz_questions_panel', array(
                'page' => $correctPage,
            ));
        }

        // Get questions for the page.
        $questions = $this->repo->getItemsInRange(
            (int)$page,
            $paginator->getMaxQuestionsPerPage()
        );

        $paginator->paginate(
            (int)$page,
            $this->repo->getQuestionsTotalCount()
        );

        return $this->render('QuizBundle:Questions:dashboard.html.twig', array(
            'questions' => $questions,
            'paginator' => $paginator,
            'form' => $form->createView(),
        ));
    }

    /**
     * Edit action.
     *
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        $this->initDoctrine();

        $question = $this->repo->find($id);

        if (!$question) {
            $this->addFlash(
                Dictionary::TYPE_FLASH_ERROR,
                Dictionary::getMessage(Dictionary::ERROR_QUESTION_NOT_FOUND)
            );

            return $this->redirectToRoute('_quiz_questions_panel');
        }

        $originalAnswers = new ArrayCollection();

        // Collect original answers.
        foreach ($question->getAnswers() as $answer) {
            $originalAnswers->add($answer);
        }

        $form = $this->createForm(QuestionEditType::class, $question);
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($originalAnswers as $answer) {
                // Compare original and new answers. Each one that
                // did not appear after form submission should be deleted.
                if (false === $question->getAnswers()->contains($answer)) {
                    $this->em->remove($answer);
                }
            }

            $this->em->flush();

            $this->addFlash(
                Dictionary::TYPE_FLASH_SUCCESS,
                Dictionary::getMessage(Dictionary::SUCCESS_QUESTION_SAVE
            ));
        }

        return $this->render('QuizBundle:Questions:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Delete action.
     *
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function deleteAction($id, Request $request)
    {
        $this->initDoctrine();

        // Get number of the last page.
        $destinationPage = $request->query->get('strona', 1);

        $question = $this->repo->find($id);

        if (!$question) {
            $this->addFlash(
                Dictionary::TYPE_FLASH_ERROR,
                Dictionary::getMessage(Dictionary::ERROR_QUESTION_DEL_NOT_FOUND, array($id))
            );
        } else {
            try {
                $this->em->remove($question);
                $this->em->flush();

                $this->addFlash(
                    Dictionary::TYPE_FLASH_SUCCESS,
                    Dictionary::getMessage(Dictionary::SUCCESS_QUESTION_DEL, array($id))
                );
            }
            catch (\Exception $e) {
                $this->addFlash(
                    Dictionary::TYPE_FLASH_ERROR,
                    Dictionary::getMessage(Dictionary::ERROR_QUESTION_DEL_GENERAL)
                );
            }
        }

        // Redirect to a previous page the user came from.
        return $this->redirectToRoute('_quiz_questions_panel', array('page' => $destinationPage));
    }

    /**
     * Fills class properties with references
     * to EntityManager and QuestionRepository objects.
     */
    private function initDoctrine()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->repo = $this->em->getRepository('QuizBundle:Question');
    }
}
