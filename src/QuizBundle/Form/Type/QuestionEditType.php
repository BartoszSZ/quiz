<?php

namespace QuizBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use QuizBundle\Form\Type\AnswerType;

class QuestionEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifier', TextType::class, array(
                'error_bubbling' => true,
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Identyfikator',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => false,
                'error_bubbling' => true,
                'attr' => array(
                    'placeholder' => 'Treść pytania',
                ),
            ))
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Jednokrotnego wyboru' => 1,
                    'Wielokrotnego wyboru' => 2,
                ),
                'expanded' => true,
                'multiple' => false,
                'error_bubbling' => true,
                'choices_as_values' => true,
                'label' => false,
            ))
            ->add('level', ChoiceType::class, array(
                'choices'  => array(
                    'Dla początkujących' => 1,
                    'Dla zaawansowanych' => 2,
                    'Dla ekspertów' => 3,
                ),
                'error_bubbling' => true,
                'choices_as_values' => true,
                'label' => false,
            ))
            ->add('status', CheckboxType::class, array(
                'label' => false,
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('answers', CollectionType::class, array(
                'entry_type' => AnswerType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => true,
                'label' => false,
            ))
            ->add('submit', SubmitType::class, array('label' => 'Zapisz'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuizBundle\Entity\Question',
            'validation_groups' => array('Default', 'Edit'),
            'error_mapping' => array(
                'activationLegal' => 'status',
            ),
        ));
    }
}
